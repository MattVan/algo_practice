/**
 * @author - Matt van Dinther
 * last edited - October 6, 2020
 * <p>
 * Practicing data structures and algorithm questions from Cracking the Coding Interview
 */

import java.util.Hashtable;
import java.util.Arrays;

public class ArraysAndStrings {

    public static void main(String[] args) {
        //#5


        //Testing #4
//
//        String inp = "Tact Coa";
//        palindromePermutator(inp);

        //Testing #3

//        String inp = "Hello there, friend    ";
//        int len = inp.length() - 4;
//        replaceSpaces(inp.toCharArray(), len);

        //Testing #2

//        String matchOne = "match";
//        String matchTwo = "hcmat";
//        String noMatch = "asdf";
//
//        System.out.println(checkPermutation(matchOne, matchTwo));
//        System.out.println(checkPermutation(matchOne, noMatch));

        //Testing #1

//        String correct = "Hi";
//        String notCorrect = "HHii TThhEErre";
//
//        System.out.println(isUnique(correct));
//        System.out.println(isUnique(notCorrect));
    }


    /*
    #5 - Check to see if one string is one or zero edits away (insert, delete, replace) from a second string

    assuming all word comparisons are case insensitive
    assuming the first string will be the "true" string, and the second one is the one we are checking for edits

    pale --> ple // insert
    pales -- > pale // removal
    pale --> bale // replace

    pale --> bake
     */

    public static void editsAway(String inpOne, String inpTwo){

        inpOne= inpOne.toLowerCase();
        inpTwo= inpTwo.toLowerCase();

        //initial check if they are the same, or they are two edits away
        if(inpOne.equals(inpTwo)){
            System.out.println("They are exactly the same");
            return;
        } else if(inpOne.length() - inpTwo.length() > 1) {
            System.out.println("They are more than one character away from each other");
            return;
        } else {
            int diffIndex = 0;
            for(int i = 0 ; i < inpOne.length(); i++){

                if(inpOne.charAt(i) != inpTwo.charAt(i)){
                    diffIndex = i;
                    break;
                }
            }

        }



        //check insert ie. pale --> ple
        for(int i = 0 ; i < inpOne.length(); i++){
            if(inpOne.charAt(i) != inpTwo.charAt(i)){

            }
        }


        //check remove

        //check replace - Lengths have to be equal

    }

    /*

    **come back to this one later**

    #4 check to see if a string has permutations which are palindromes not limited to dictionary words.
    Print all the palindromes you find

    Assumptions - Case insensitive

    //Find how many characters in the string have even amounts, and the input must only be of odd length if we have
    //many even numbered characters and only one odd numbered char (ie aasdd)
     */

    public static void palindromePermutator(String inp){

        //pull apart input into lowercase character array and sort to make the next step easier.

        char[] sorted = inp.toLowerCase().toCharArray();
        Arrays.sort(sorted);

        System.out.println(sorted);

        //find unique character instances to use and count

        int count = 0;

        for(int i = 0; i < sorted.length; i++){
            int o = sorted[i];
            System.out.println(sorted[i] + " " + o);
        }

        //print result array

    }

    /*
    #3 replace all instances of spaces in a string with "%20"
    assumptions given - If in java, use char array which will contain blank spaces at the end to allow for extra characters
    without overflow. You are given the "true" length of the string (ie, not the spaces at the end)
    */
    public static void replaceSpaces(char[] inp, int len) {

        //First thought is to just loop through the array (O(N) t.complex)
        //There is another method to start at the end and work backward counting the spaces so you can short circuit.
        // I'm unsure this is more efficient, as you have to run through pieces of the character array twice instead
        // of just running through once and replacing to build a result string or char array
        String result = "";
        for (int i = 0; i < len; i++) {
            if (inp[i] == ' ') {
                result += "%20";
            } else {
                result += inp[i];
            }
        }
        System.out.println(result);
    }
    /*
    #2
    Given two strings, write a method to decide if one is a permutation (rearrangement) of the other.

    Wish I knew before - Sort is a void method, so individual calls to it must be made instead of inserting them into
    other methods as it will produce an error
    */


    public static boolean checkPermutation(String strOne, String strTwo) {
        char[] arr1 = strOne.toCharArray();
        char[] arr2 = strTwo.toCharArray();

        //pre sort
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));

        Arrays.sort(arr1);
        Arrays.sort(arr2);

        //post sort
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));

        //Sort the two as character arrays, if they are equal then they are permutations of each other.
        return Arrays.equals(arr1, arr2);
    }

    /*
    Determine if a string has duplicate characters
    This solution should be faster because we have an O(1) time complexity on average for search and insert using
    of hashtable data structures.

    Should add a method to increase(rehash) the table if it gets to capacity, but for the purposes of this method I
    won't be using strings that are too long.
    */
    public static boolean isUnique(String inp) {

        Hashtable<Integer, Character> charTable = new Hashtable<Integer, Character>();

        //initialize hash table with first character
        charTable.put(0, inp.charAt(0));

        for (int i = 1; i < inp.length(); i++) {

            if (!charTable.containsValue(inp.charAt(i))) {
                charTable.put(i, inp.charAt(i));
            } else {
                //If the value already exists, we can just return false
//                System.out.println(charTable.toString());
                return false;
            }
        }

        return true;
    }

}